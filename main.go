package main



import (

	"encoding/csv"
	"flag"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/tealeg/xlsx"

	"statistics/controller"
)


const COMMAND_LINE_ERROR_MESSAGE = 	`You have to start program with three arguments 
										( type of document [ 1 for coins spent, 2 for number of plays, 3 for revenue ],
										start date,
										end date [ optional, in case nothing is put it will calculate till Now ] )`


var delimiter = flag.String("d", ",", "Delimiter for felds in the CSV input.")

func main() {

    commandLineArgs := os.Args[ 1 : ]

    if len( commandLineArgs ) < 2 {
    	log.Println( COMMAND_LINE_ERROR_MESSAGE )
    	return	
    }

    typeOfDocument, err := strconv.Atoi( commandLineArgs[ 0 ] )
    if err != nil {
    	log.Println( "Cannot convert command line argument to int ( type of document )" )
    	log.Println( COMMAND_LINE_ERROR_MESSAGE )
    	return
    }


    //layout := "2006-01-02T15:04:05.000Z"
    layout := "2006-01-02"

	fromTime, err := time.Parse( layout, commandLineArgs[ 1 ] )
	if err != nil {
    	log.Println( "Cannot convert command line argument to date ( start date )" )
    	log.Println( COMMAND_LINE_ERROR_MESSAGE )
    	return
    }


    // in case there is no argument use today as limit
	toTime := time.Now()
	if len( commandLineArgs ) >= 3 {
		toTime, err = time.Parse( layout, commandLineArgs[ 2 ] )

		if err != nil {
    		log.Println( "Cannot convert command line argument to date ( end date )" )
    		log.Println( COMMAND_LINE_ERROR_MESSAGE )
    		return
    	}
	}


	// cheks dates order
	if fromTime.After( toTime ) {

    	log.Println( "Start date should be before end date" )
    	log.Println( COMMAND_LINE_ERROR_MESSAGE )
    	return

	}

	controller.ExportToFileData( fromTime, toTime, typeOfDocument )

	flag.Parse()
	err = generateXLSXFromCSV( "output.csv", "output.xlsx", *delimiter)
	if err != nil {
		log.Printf( err.Error() )
		return
	}

}


// Generates XLSX file from CSV file
func generateXLSXFromCSV(csvPath string, XLSXPath string, delimiter string) error {
	csvFile, err := os.Open( csvPath )
	if err != nil {
		return err
	}
	defer csvFile.Close()
	reader := csv.NewReader(csvFile)
	if len(delimiter) > 0 {
		reader.Comma = rune(delimiter[0])
	} else {
		reader.Comma = rune(';')
	}
	xlsxFile := xlsx.NewFile()
	sheet, _ := xlsxFile.AddSheet(csvPath)
	fields, err := reader.Read()
	for err == nil {
		row := sheet.AddRow()
		for _, field := range fields {
			cell := row.AddCell()
			cell.Value = field
		}
		fields, err = reader.Read()
	}
	if err != nil {
		log.Printf(err.Error())
	}
	return xlsxFile.Save(XLSXPath)
}
