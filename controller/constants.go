package controller


import (
	"gopkg.in/mgo.v2"
	"os"
)

const (

	TournamentsDB  			= "tournaments"	
	usercollection 			= "users"
	spinlogscollection  	= "spinlogs"
	transactionscollection  = "transactions"


	// used to 
	COINS_SPENT 	= 1
	NUMBER_OF_PLAYS = 2
	REVENUE 		= 3
	FULL_DOCUMENT 	= 4

)


var (
	MgoSession              *mgo.Session
	Mongoserver             = os.Getenv("MGOSERVER")
	Mogouser                = os.Getenv("MGOUSER")
	Mogopass                = os.Getenv("MGOPASS")
)
