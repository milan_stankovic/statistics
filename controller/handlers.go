package controller



import (

	"encoding/csv"
	_ "errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"os"
	_ "reflect"
	"strconv"
	"time"
)


type Day struct {
    Start 	time.Time
    End 	time.Time
}


type ResultsOut struct {
	AmountBet 		int64 	`bson:"AmountBet"`
	AmountWon   	int64 	`bson:"AmountWon"`
	NumberOfSpins 	int64 	`bson:"NumberOfSpins"`
	Time 			Day 	`bson:"Day"`
}

type ResultSegmentData struct {

	Registered 	ResultsOut 
	Guests 		ResultsOut
	Total 		ResultsOut
}

type RevenueSegmentData struct {

	Money 					uint64 	`bson:"Money" json:"Money"`
	NumberOfTransactions 	int64 	`bson:"NumberOfTransactions" json:"NumberOfTransactions"`
	Coins 					int64	`bson:"Coins" json:"Coins"`
	Time 					Day 	`bson:"Day"`
}



// Exports data to CSV file names output.csv locally, type of document / format determined by third parameter
func ExportToFileData( fromDate time.Time, toDate time.Time, typeOfDocument int ) {

	csvfile, err := os.Create( "output.csv" )
	if err != nil {
		log.Println( "ExportToFileData error: ", err.Error() )
	    return
	}
	defer csvfile.Close()



	var records [][]string
	writer := csv.NewWriter( csvfile )


	// choose which data should be presented to user
	if typeOfDocument == COINS_SPENT {

		data, err := coinsSpent( fromDate, toDate )
		if err != nil {
			log.Println("Error:", err)
	    	return
		}
		records = convertCoinsSpentToCSV( data )

	} else if typeOfDocument == NUMBER_OF_PLAYS {

		data, err := coinsSpent( fromDate, toDate )
		if err != nil {
			log.Println("Error:", err)
	    	return
		}
		
		records = convertNumberOfPlaysToCSV( data )
	
	} else if typeOfDocument == FULL_DOCUMENT {
		data, err := coinsSpent( fromDate, toDate )
		if err != nil {
			log.Println("Error:", err)
	    	return
		}

		records = convertFullDocumentToCSV( data )

	} else if typeOfDocument == REVENUE {
		data, err := revenue( fromDate, toDate )
		if err != nil {
			log.Println("Error:", err)
	    	return
		}


		records = convertRevenueToCSV( data )
	}


	// writes data to file 
	for _, record := range records {
		err := writer.Write(record)
		if err != nil {
			log.Println("Error:", err)
			return
		}
	}
	writer.Flush()
}


// Returns array of data for every day between passed parameters
func coinsSpent( fromDate time.Time, toDate time.Time ) ( []ResultSegmentData, error ) {
	var result []ResultSegmentData

	days, err := generateDaysBetween( fromDate, toDate ) 
	if err != nil  {
		log.Println( "Error in days creating: " + err.Error() )
		return nil, err
	}



	for index, day := range( days ) {
		segment, err :=  totalCoinsSpent( day.Start, day.End )
		log.Println( "Executing: ", index )
		if err != nil {
			log.Println( "Error in executing totalCoinsSpent for day:", index  )
			return nil, err
		}

		result = append( result, *segment )
	}

	return result, nil

}

// Returns data for day ( passed parameters are in upper always called in day)
func totalCoinsSpent( fromDate time.Time, toDate time.Time ) ( *ResultSegmentData, error ) {


	currentDay := Day { Start: fromDate, End: toDate }


	log.Println( "TotalCoinsSpent entered" )

	//mongo
	MgoSession, err := mgo.Dial( Mongoserver )
	if err != nil {
		//panic(err)
		log.Println( "TotalCoinsSpent: error in connecting to database: " + err.Error() )
		return nil, err
	}
	defer MgoSession.Close()

	MgoSession.SetMode( mgo.Monotonic, true )


	usersC := MgoSession.DB( TournamentsDB ).C( usercollection )
	spinlogsC := MgoSession.DB( TournamentsDB ).C( spinlogscollection )


	// ---------- trying to add indexes on fields ----------
	index := mgo.Index{
		Key: []string{"results.amountbet"},
	}
	if err = spinlogsC.EnsureIndex(index); err != nil {
		log.Println("Error in indexing database")
	}

	index = mgo.Index{
		Key: []string{"results.amountwon"},
	}
	if err = spinlogsC.EnsureIndex(index); err != nil {
		log.Println("Error in indexing database")
	}
	// -----------------------------------------------------


	var players []bson.ObjectId
	var guests []bson.ObjectId

	var guestsTemp []interface{}
	var playersTemp []map[string]interface{}

	err = spinlogsC.Find( bson.M{ "create_at": bson.M{ "$gte": fromDate, "$lt": toDate } } ).Select( bson.M{ "player._id" : 1 } ).All(  &playersTemp )
	if err != nil {
    	log.Println( "TotalCoinsSpent: error in collecting all ids from spinlogs: " + err.Error() )
		return nil, err
	}

	for _, p := range( playersTemp ) {
		tttt := p["player"]
		if tttt != nil {
			a := tttt.( map[string]interface{} )
			ttid := a["_id"]
			players = append( players, ttid.(bson.ObjectId) )
		}
	}

	log.Println( "Ids number:" )
	log.Println( len( players ) )

	removeDuplicates( &players )

	log.Println( len( players ) )


	log.Println( "Executed" )
	log.Println( len( players ) )


	conditions := bson.M{"$and": []bson.M{ 	bson.M{ "_id": bson.M{ "$in": players }},
											bson.M{"firstname": "Guest"},
											bson.M{"lastname": "_"},
											bson.M{"fbid": ""}, } }

	err = usersC.Find( conditions ).Select( bson.M{"_id": 1 } ).All(  &guestsTemp )
	if err != nil {
    	log.Println( "TotalCoinsSpent: error in finding ids of guests from users: " + err.Error() )
		return nil, err
	}

	for _, p := range( guestsTemp ) {
		if p != nil {
			a := p.( bson.M )
			ttid := a["_id"]
			guests = append( guests, ttid.(bson.ObjectId) )
		}
	}

	removeDuplicates( &guests )


	resultsGuests, resultsTotal := []ResultsOut{}, []ResultsOut{}
	// ----------------------------- calculating for guests -----------------------------
	q1 := bson.M{}
	q1["create_at"] = bson.M{"$gte": fromDate, "$lt": toDate }
	q1["player._id"] = bson.M{ "$in": guests }
	qAgreg1 := bson.M{}
	if ( len( q1 ) > 1 ) {
		qAgreg1 = bson.M{"$and": []bson.M{ q1 } }
	} else {
		qAgreg1 = q1
	}

	pipeline1 := []bson.M{
		bson.M{"$match": qAgreg1},
		//bson.M{"$unwind": "$results"},
		//bson.M{"$group": bson.M{"_id": "$_id", "AmountBet": bson.M{"$sum": "$results.amountbet"}, "AmountWon": bson.M{"$sum": "$results.amountwon"}}},
		bson.M{"$group": bson.M{"_id": "-", "AmountBet": bson.M{"$sum": "$results.amountbet"}, "AmountWon": bson.M{"$sum": "$results.amountwon"}, "NumberOfSpins": bson.M{"$sum": 1}}},
	

	}
	pipe1 := spinlogsC.Pipe( pipeline1 )

	if err := pipe1.All( &resultsGuests ); err != nil {
    	log.Println( "TotalCoinsSpent: error in summarizing for guests: " + err.Error() )
		return nil, err
	}
	// ----------------------------------------------------------------------------------


	// ----------------------------- calculating for all -----------------------------
	q2 := bson.M{}
	q2["create_at"] = bson.M{"$gte": fromDate, "$lt": toDate }
	qAgreg2 := bson.M{}
	if ( len( q2 ) > 1 ) {
		qAgreg2 = bson.M{"$and": []bson.M{ q2 } }
	} else {
		qAgreg2 = q2
	}

	pipeline2 := []bson.M{
		bson.M{"$match": qAgreg2},
		//bson.M{"$unwind": "$results"},
		bson.M{"$group": bson.M{"_id": "-", "AmountBet": bson.M{"$sum": "$results.amountbet"}, "AmountWon": bson.M{"$sum": "$results.amountwon"}, "NumberOfSpins": bson.M{"$sum": 1}}},
		
	}
	pipe2 := spinlogsC.Pipe( pipeline2 )


	if err := pipe2.All( &resultsTotal ); err != nil {
		log.Println( "TotalCoinsSpent: error in summarizing all: " + err.Error() )
		return nil, err
	}
	// ----------------------------------------------------------------------------------



	// log.Println( resultsGuests )
	// log.Println( resultsTotal )
	//log.Println( reflect.TypeOf( resultsTotal[0] ) )
		

	gRes := ResultsOut{ AmountBet: 0, AmountWon: 0, NumberOfSpins: 0, Time: currentDay }
	tRes := ResultsOut{ AmountBet: 0, AmountWon: 0, NumberOfSpins: 0, Time: currentDay }
	if len( resultsGuests ) > 0 {
		gRes.AmountBet 		= resultsGuests[0].AmountBet
		gRes.AmountWon 		= resultsGuests[0].AmountWon
		gRes.NumberOfSpins 	= resultsGuests[0].NumberOfSpins
	}
	if len( resultsTotal ) > 0 {
		tRes.AmountBet 		= resultsTotal[0].AmountBet
		tRes.AmountWon 		= resultsTotal[0].AmountWon
		tRes.NumberOfSpins 	= resultsTotal[0].NumberOfSpins
	}
	uRes := ResultsOut{ AmountBet : tRes.AmountBet - gRes.AmountBet, AmountWon : tRes.AmountWon - gRes.AmountWon, NumberOfSpins : tRes.NumberOfSpins - gRes.NumberOfSpins, Time: currentDay }


	log.Println( "TotalCoinsSpent ended" )


	// log.Println(ResultSegmentData{ Registered : uRes, Guests : gRes, Total : tRes })

	return &ResultSegmentData{ Registered : uRes, Guests : gRes, Total : tRes }, nil 
}

// Returns array of data for every day between passed parameters
func revenue( fromDate time.Time, toDate time.Time ) ( []RevenueSegmentData, error ) {


	var result []RevenueSegmentData

	days, err := generateDaysBetween( fromDate, toDate ) 
	if err != nil  {
		log.Println( "Error in days creating: " + err.Error() )
		return nil, err
	}



	for index, day := range( days ) {
		segment, err :=  revenueData( day.Start, day.End )
		log.Println( "Executing: ", index )
		if err != nil {
			log.Println( "Error in executing totalCoinsSpent for day:", index  )
			return nil, err
		}

		result = append( result, *segment )
	}

	return result, nil

}


// Generates revenue between oassed date range
func revenueData( fromDate time.Time, toDate time.Time ) ( *RevenueSegmentData, error ) {

	currentDay := Day { Start: fromDate, End: toDate }

	log.Println( "revenueData entered" )

	//mongo
	MgoSession, err := mgo.Dial( Mongoserver )
	if err != nil {
		//panic(err)
		log.Println( "TotalCoinsSpent: error in connecting to database: " + err.Error() )
		return nil, err
	}
	defer MgoSession.Close()

	MgoSession.SetMode( mgo.Monotonic, true )

	transactionsC := MgoSession.DB( TournamentsDB ).C( transactionscollection )


	// ---------- trying to add indexes on fields ----------
	// index := mgo.Index{
	// 	Key: []string{"results.amountbet"},
	// }
	// if err = spinlogsC.EnsureIndex(index); err != nil {
	// 	log.Println("Error in indexing database")
	// }

	// index = mgo.Index{
	// 	Key: []string{"results.amountwon"},
	// }
	// if err = spinlogsC.EnsureIndex(index); err != nil {
	// 	log.Println("Error in indexing database")
	// }
	// -----------------------------------------------------


	results := []RevenueSegmentData{}
	// ----------------------------- calculating revenues -----------------------------
	q1 := bson.M{}
	q1["purchasedTime"] = bson.M{"$gte": fromDate, "$lt": toDate }
	// qAgreg1 := bson.M{}
	// if ( len( q1 ) > 1 ) {
	// 	qAgreg1 = bson.M{"$and": []bson.M{ q1 } }
	// } else {
	// 	qAgreg1 = q1
	// }

	pipeline1 := []bson.M{
		bson.M{"$match": q1},
		//bson.M{"$unwind": "$package"},
		bson.M{"$group": bson.M{"_id": "-", "Coins": bson.M{"$sum": "$package.coins"}, "Money": bson.M{"$sum": "$package.price"}, "NumberOfTransactions": bson.M{"$sum": 1}}},
	

	}
	pipe1 := transactionsC.Pipe( pipeline1 )

	if err := pipe1.All( &results ); err != nil {
    	log.Println( "revenueData: error in summarizing revenues: " + err.Error() )
		return nil, err
	}
	// ----------------------------------------------------------------------------------

	temp := RevenueSegmentData { Money : 0, NumberOfTransactions : 0, Coins : 0, Time : currentDay }


	if len( results ) > 0 {
		temp.Money 					= results[0].Money
		temp.NumberOfTransactions 	= results[0].NumberOfTransactions
		temp.Coins 					= results[0].Coins
	}

	log.Println( "revenueData ended" )

	return &temp, nil 
}



// Returns array of days between two given parameters
func generateDaysBetween( fromDate time.Time, toDate time.Time ) ( []Day, error ) {

	var result []Day
	var DAY_IN_NANOSECONDS = time.Duration( 24 * 60 * 60 * 1000 * 1000 * 1000 )

	fromNormalized := time.Date( fromDate.Year(), fromDate.Month(), fromDate.Day(), 0, 0, 0, 0, time.UTC)
	toNormalized := time.Date( toDate.Year(), toDate.Month(), toDate.Day(), 0, 0, 0, 1, time.UTC).Add( DAY_IN_NANOSECONDS )

	iterFirst := fromNormalized
	iterSecond := fromNormalized.Add( DAY_IN_NANOSECONDS )


	for iterSecond.Before( toNormalized ) {
		result = append( result,  Day{ Start : iterFirst, End : iterSecond } )

		iterFirst = iterSecond
		iterSecond = iterSecond.Add( DAY_IN_NANOSECONDS ) 
	}

	//log.Println( result )

	return result, nil

}


// Creates formatted data ( all data ) for CSV writter
func convertFullDocumentToCSV( data []ResultSegmentData ) [][]string {
	var result [][]string

	headers := []string { 	"Registered Amount Bet",
							"Registered Amount Won",
							"Registered Number Of Spins",
							"Guest Amount Bet",
							"Guest Amount Won",
							"Guest Number Of Spins",
							"Total Amount Bet",
							"Total Amount Won",
							"Total Number Of Spins",
							"Start Date"}

	result = append( result, headers )

	for _, d := range( data ) {
		temp := []string{ 	strconv.FormatInt( d.Registered.AmountBet, 10 ),
						strconv.FormatInt( d.Registered.AmountWon, 10 ),
						strconv.FormatInt( d.Registered.NumberOfSpins, 10 ),

						strconv.FormatInt( d.Guests.AmountBet, 10 ),
						strconv.FormatInt( d.Guests.AmountWon, 10 ),
						strconv.FormatInt( d.Guests.NumberOfSpins, 10 ),

						strconv.FormatInt( d.Total.AmountBet, 10 ),
						strconv.FormatInt( d.Total.AmountWon, 10 ),
						strconv.FormatInt( d.Total.NumberOfSpins, 10 ),

						d.Registered.Time.Start.String() }
		result = append( result, temp )
	}

	return result
}


// Creates formatted data ( all data ) for CSV writter
func convertCoinsSpentToCSV( data []ResultSegmentData ) [][]string {
	var result [][]string

	headers := []string { 	"Registered Amount Bet",
							"Registered Amount Won",
							"Total Amount Bet",
							"Total Amount Won",
							"Start Date"}

	result = append( result, headers )

	for _, d := range( data ) {
		temp := []string{ 	strconv.FormatInt( d.Registered.AmountBet, 10 ),
							strconv.FormatInt( d.Registered.AmountWon, 10 ),

							strconv.FormatInt( d.Total.AmountBet, 10 ),
							strconv.FormatInt( d.Total.AmountWon, 10 ),

							d.Registered.Time.Start.String() }

		result = append( result, temp )
	}

	return result
}


// Creates formatted data ( all data ) for CSV writter
func convertNumberOfPlaysToCSV( data []ResultSegmentData ) [][]string {
	var result [][]string

	headers := []string { 	"Registered Number Of Spins",
							"Total Number Of Spins",
							"Start Date"}

	result = append( result, headers )

	for _, d := range( data ) {
		temp := []string{ 	strconv.FormatInt( d.Registered.NumberOfSpins, 10 ),
							strconv.FormatInt( d.Total.NumberOfSpins, 10 ),

							d.Registered.Time.Start.String() }

		result = append( result, temp )
	}

	return result
}


// Creates formatted data from revenue data for CSV Writter
func convertRevenueToCSV( data []RevenueSegmentData ) [][]string {
	var result [][]string

	headers := []string { 	"Coins Purchased",
							"Revenue",
							"Number Of Transactions",
							"Start Date"}

	result = append( result, headers )

	for _, d := range( data ) {
		temp := []string{ 	strconv.FormatInt( d.Coins, 10 ),
							convertCentsToDollars( d.Money ),
							strconv.FormatInt( d.NumberOfTransactions, 10 ),

							d.Time.Start.String() }

		result = append( result, temp )
	}

	return result
}

// removes duplicates from array of ids
func removeDuplicates(xs *[]bson.ObjectId ) {
	found := make(map[ bson.ObjectId ]bool)
	j := 0
	for i, x := range *xs {
		if !found[ x ] {
			found[ x ] = true
			( *xs )[ j ] = ( *xs )[ i ]
			j++
		}
	}
	*xs = ( *xs )[ : j ]
}



// Converts of cents to more human readable form in dollars
func convertCentsToDollars( cents uint64 ) string {

	p := strconv.FormatUint( cents, 10 )
	if len( p ) == 1 {
		p = "0.0" + p
	} else if len( p ) == 2 {
		p = "0." + p
	} else {
		pointIndex := len( p ) - 2
		p = p[:pointIndex] + "." + p[pointIndex :]
	}

	return p
}

